package com.funds.wallet.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.funds.wallet.R;
import com.funds.wallet.adapter.PaymentCardAdapter;
import com.funds.wallet.dynamiclistview.DynamicListView;
import com.funds.wallet.model.PaymentCard;

import java.util.ArrayList;

/**
 * Created by Junseong on 2015-05-23.
 */
public class TabPageFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tab, container, false);

        setUpUI(inflater, root);

        return root;
    }

    private void setUpUI(LayoutInflater inflater, View root) {
        ArrayList<PaymentCard> cardList = new ArrayList<>();
        addPaymentCard(cardList);

        PaymentCardAdapter cardAdapter = new PaymentCardAdapter(inflater, cardList);
        cardAdapter.updateCardList(cardList);
        DynamicListView dynamicListView = (DynamicListView) root.findViewById(R.id.list_view);

        dynamicListView.setArrayList(cardList);
        dynamicListView.setAdapter(cardAdapter);
        dynamicListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void addPaymentCard(ArrayList<PaymentCard> cardList) {
        cardList.add(new PaymentCard(R.drawable.payment_card_image1));
        cardList.add(new PaymentCard(R.drawable.payment_card_image2));
        cardList.add(new PaymentCard(R.drawable.payment_card_image3));
        cardList.add(new PaymentCard(R.drawable.payment_card_image4));
        cardList.add(new PaymentCard(R.drawable.payment_card_image5));
    }
}