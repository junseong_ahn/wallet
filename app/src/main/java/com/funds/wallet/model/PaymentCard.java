package com.funds.wallet.model;

/**
 * Created by Junseong on 2015-05-24.
 */
public class PaymentCard {
    private int mCardType;
    private int mCardNumber;
    private int mExpiredMonth;
    private int mSecurityNum;
    private String mLastName;
    private String mFirstName;
    private int mCardImageResource;

    public PaymentCard(int cardImageResource) {
        setCardImageResource(cardImageResource);
    }

    public int getCardImageResource() {
        return mCardImageResource;
    }

    public void setCardImageResource(int cardImageResource) {
        this.mCardImageResource = cardImageResource;
    }
}
