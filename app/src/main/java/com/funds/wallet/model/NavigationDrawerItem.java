package com.funds.wallet.model;

public class NavigationDrawerItem {
    private int mTitle;
    private int mIconResource;

    public NavigationDrawerItem(int title, int cardImageResource) {
        setTitle(title);
        setIconResource(cardImageResource);
    }

    public int getIconResource() {
        return mIconResource;
    }

    public void setIconResource(int iconResource) {
        this.mIconResource = iconResource;
    }

    public int getTitle() {
        return mTitle;
    }

    public void setTitle(int title) {
        mTitle = title;
    }
}
