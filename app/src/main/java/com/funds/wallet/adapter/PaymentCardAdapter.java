package com.funds.wallet.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.funds.wallet.R;
import com.funds.wallet.model.PaymentCard;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Create only the necessary number of views to fill the screen, and reuse these views as soon as they disappear.
 */
public class PaymentCardAdapter extends BaseAdapter {
    private final int INVALID_ID = -1;
    private final LayoutInflater mLayoutInflater;
    private List<PaymentCard> mCardList = Collections.emptyList();
    private HashMap<PaymentCard, Integer> mIdMap = new HashMap<>();

    public PaymentCardAdapter(LayoutInflater layoutInflater, List<PaymentCard> cardObject) {
        mLayoutInflater = layoutInflater;
        for (int i = 0; i < cardObject.size(); ++i) {
            mIdMap.put(cardObject.get(i), i);
        }
    }

    public void updateCardList(List<PaymentCard> cardList) {
        ThreadPreconditions.checkOnMainThread();
        this.mCardList = cardList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mCardList.size();
    }

    // getItem(int) in Adapter returns Object but we can override
    // it to BananaPhone thanks to Java return type covariance
    @Override
    public PaymentCard getItem(int position) {
        return mCardList.get(position);
    }

    // getItemId() is often useless, I think this should be the default
    // implementation in BaseAdapter
    @Override
    public long getItemId(int position) {
        if (position < 0 || position >= mIdMap.size()) {
            return INVALID_ID;
        }
        PaymentCard item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            view = mLayoutInflater
                    .inflate(R.layout.item_view_payment_card, parent, false);
            viewHolder.cardImageView = (ImageView) view.findViewById(R.id.payment_card_image_view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        PaymentCard cardItem = getItem(position);
        viewHolder.cardImageView.setImageResource(cardItem.getCardImageResource());

        return view;
    }

    /**
     * ViewHolder are used to to store the inflated views in order to recycle them
     */
    class ViewHolder {
        ImageView cardImageView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
