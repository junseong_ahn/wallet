package com.funds.wallet.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.funds.wallet.R;
import com.funds.wallet.model.NavigationDrawerItem;

import java.util.Collections;
import java.util.List;

public class NavigationDrawerAdapter extends BaseAdapter {
    private List<NavigationDrawerItem> mDrawerItemList = Collections.emptyList();
    private final LayoutInflater mLayoutInflater;

    public NavigationDrawerAdapter(LayoutInflater layoutInflater, List<NavigationDrawerItem> drawerItemObject) {
        mLayoutInflater = layoutInflater;
        mDrawerItemList = drawerItemObject;
    }

    @Override
    public int getCount() {
        return mDrawerItemList.size();
    }

    // getItem(int) in Adapter returns Object but we can override
    // it to BananaPhone thanks to Java return type covariance
    @Override
    public NavigationDrawerItem getItem(int position) {
        return mDrawerItemList.get(position);
    }

    // getItemId() is often useless, I think this should be the default
    // implementation in BaseAdapter
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            view = mLayoutInflater
                    .inflate(R.layout.item_navigation_drawer, parent, false);
            viewHolder.iconImageView = (ImageView) view.findViewById(R.id.icon_image_view);
            viewHolder.titleTextView = (TextView) view.findViewById(R.id.title_text_view);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }

        NavigationDrawerItem drawerItem = getItem(position);
        viewHolder.iconImageView.setImageResource(drawerItem.getIconResource());
        viewHolder.titleTextView.setText(drawerItem.getTitle());

        return view;
    }

    /**
     * ViewHolder are used to to store the inflated views in order to recycle them
     */
    class ViewHolder {
        TextView titleTextView;
        ImageView iconImageView;
    }

}