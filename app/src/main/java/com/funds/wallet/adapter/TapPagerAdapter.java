package com.funds.wallet.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.funds.wallet.fragment.TabPageFragment;

public class TapPagerAdapter extends FragmentStatePagerAdapter {
    private SparseArray<Fragment> mRegisteredFragments = new SparseArray<>();
    private CharSequence mTitles[];
    private int mNumberOfTabs;

    public TapPagerAdapter(FragmentManager fm, CharSequence titles[], int numberOfTabs) {
        super(fm);
        mTitles = titles;
        mNumberOfTabs = numberOfTabs;
    }

    @Override
    public int getCount() {
        return mNumberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        TabPageFragment tabPageFragment = new TabPageFragment();
        return tabPageFragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        mRegisteredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        mRegisteredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return mRegisteredFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}
