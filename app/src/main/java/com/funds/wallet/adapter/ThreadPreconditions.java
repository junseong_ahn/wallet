package com.funds.wallet.adapter;

import android.os.Looper;

import com.funds.wallet.BuildConfig;

/**
 * Make sure your adapter is used only from one thread, the Main thread.
 */
public class ThreadPreconditions {
    public static void checkOnMainThread() {
        if (BuildConfig.DEBUG) {
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                throw new IllegalStateException("This method should be called from the Main Thread");
            }
        }
    }
}
